jQuery(document).ready(function() {
    var w_liner=jQuery('.raidraptor_w');
    var x_liner=jQuery('.raidraptor_x');
    var y_liner=jQuery('.raidraptor_y');
    var z_liner=jQuery('.raidraptor_z');
    var cmd_console=jQuery('#raidraptor_cmd_console');
    var _LOGIN_HEIGHT=240;
    var _LOGIN_WIDTH=560;
    var _DOC_HEIGHT=jQuery(document).height();
    var _DOC_WIDTH=jQuery(document).width();
    var _DIM_Y=100;
    var _DIM_X=100;
    var _TIMER=10;
    
    var _ANIM_WIDTH=_DOC_WIDTH-_LOGIN_WIDTH;
    var _ANIM_HEIGHT=_DOC_HEIGHT-_LOGIN_HEIGHT;
    var animator=setInterval(function() {
        _DIM_Y=parseInt(_DIM_Y+(0.25 * _DIM_Y));
        _DIM_X=parseInt(_DIM_X+(0.25 * _DIM_X));
        _TIMER=_TIMER * 1.25;
        w_liner.animate({'height': _DIM_Y+'px'});
        y_liner.animate({'height': _DIM_Y+'px'});
        x_liner.animate({'width': _DIM_X+'px'});
        z_liner.animate({'width': _DIM_X+'px'});
        if (_DIM_X>=_ANIM_WIDTH || _DIM_Y>=_ANIM_HEIGHT) {
            clearInterval(animator);
            w_liner.fadeOut(200);
            x_liner.fadeOut(200);
            y_liner.fadeOut(200);
            z_liner.fadeOut(200, function() {
                cmd_console.fadeIn(300);
            });
        }            
    }, _TIMER);
    
});