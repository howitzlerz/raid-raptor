from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'raidraptor.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'revolution_falcon.views.index', name='home'),
    url(r'^test/', 'revolution_falcon.views.home', name='test'),
	url(r'^file/(?P<file_short_url>\w+)', 'revolution_falcon.views.file_manager', name='file manager'),
    
    #--- STOCKS
    url(r'^stock/get/(?P<param_max_result>[0-9]+)/$', 'revolution_falcon.views.stock_service_get', name='stock get'),
    url(r'^stock/get/$', 'revolution_falcon.views.stock_service_get', name='stock get'),
    url(r'^stock/save/(?P<param_stock_value>[0-9.]+)/(?P<param_stock_change>[0-9.]+)/$', 'revolution_falcon.views.stock_service_post', name='stock post'),
    #--- STOCKS
)
#----- Addded code from legacy
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns();
#----- Added code from legacy