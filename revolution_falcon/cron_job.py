#--- CRON IMPORTS
from django_cron import CronJobBase, Schedule;
#--- URLLIB IMPORTS
from urllib.request import urlopen, Request;
from urllib.parse import urlencode;
import bs4, json;

#--- STOCK CONSTANTS
QUERY_SIZE=20;
STOCK_SERVER='http://raidraptor.herokuapp.com';
STOCK_GET_URL='{}/stock/get/{}'.format(STOCK_SERVER, QUERY_SIZE);
STOCK_POST_URL='{}/stock/save/{value}/{change}';
#--- STOCK CONSTANTS

#--- STOCK CRON
class Stock_CronJob(CronJobBase):
    RUN_EVERY_MINS= 0;#Every 3 hours
    schedule=Schedule(run_every_mins=RUN_EVERY_MINS);
    code='revolution_falcon.Stock_CronJob';
    
    def do(self):
        print("Executing stock cron");
        try:
            _server_response=urlopen('http://finance.yahoo.com/q?s=N');
            _server_soup=bs4.BeautifulSoup(_server_response.read(), 'html5lib');
            _stock_value=_server_soup.find(id="yfs_l84_n").text.strip();
            _stock_change=_server_soup.find(id="yfs_c63_n").text.strip();
            _server=urlopen(STOCK_POST_URL.format(STOCK_SERVER, value=_stock_value, change=_stock_change));
            _server_response=json.loads(_server.read().decode('UTF-8'))
            if _server_response['success']:
                print("Stock has been updated.");
            else:
                print("Stock failed to update.");
        except Exception as e:
            print(e);