#--- IMPORTS
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.http.response import HttpResponseRedirect
from django.http import HttpResponse, JsonResponse;
from django.views.decorators.csrf import ensure_csrf_cookie
from revolution_falcon.models import FileManager, StockRecord;
from django.forms import formset_factory;
import datetime
#--- IMPORTS

# Create your views here.

#--- INDEX
def index(request):
    return render_to_response('index.html', {}, RequestContext(request));
#--- INDEX

#--- HOME
def home(request):
    return render_to_response('test.html', {}, RequestContext(request));
#--- HOME

#--- STOCK: GET
def stock_service_get(request, param_max_result=20):
    _stock_list=[];
    _stock_obj=StockRecord.objects.all().order_by('-stock_id');
    _ctr=0;
    for o in _stock_obj:
        _stock_list.append({
            'value': o.stock_value,
            'change': o.stock_change,
        });
        _ctr=_ctr+1;
        if _ctr >= param_max_result:
            break;
    return JsonResponse({'result': _stock_list});
#--- STOCK: GET

#--- STOCK: POST
def stock_service_post(request, param_stock_value=None, param_stock_change=None):
    if param_stock_value and param_stock_change:
        _stock_obj=StockRecord(
            stock_value=param_stock_value,
            stock_change=param_stock_change,
        );
        _stock_obj.save();
    return JsonResponse({'success': True});
#--- STOCK: POST


#--- FILE MANAGER: GET
def file_manager(request, file_short_url):
	if isNullOrEmpty(file_short_url):
		return HttpResponse('Invalid URL!');
	else:
		_my_file=FileManager.objects.all().filter(fm_short_url=file_short_url);
		if len(_my_file)>0:
			_my_file=_my_file[0];
			_my_file.fm_total_download=_my_file.fm_total_download+1;
			_my_file.save();
			return HttpResponseRedirect(_my_file.fm_file.url.replace('raidraptor', ''));
		else:
			return HttpResponse('No such file exists.');
#--- FILE MANAGER: GET

#---------------- HELPER FUNCTIONS
def isNullOrEmpty(data):
	return data==None or data=='';
    
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip;