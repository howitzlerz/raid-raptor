from django.db import models

# Create your models here.
class FileManager(models.Model):
	fm_id=models.AutoField(primary_key=True, verbose_name='File ID', unique=True);
	fm_name=models.CharField(max_length=32, verbose_name='File Name');
	fm_group_name=models.CharField(max_length=32, choices=(('Document', 'Document'), ('Video', 'Video'), ('Music', 'Music'), ('Picture', 'Picture'), ('Others', 'Others')), blank=True)
	fm_short_url=models.CharField(max_length=127, verbose_name='File URL', blank=True);
	fm_file=models.FileField(upload_to='raidraptor/templates/files/', verbose_name='File');
	fm_password=models.CharField(verbose_name='Password', max_length=32, blank=True, default='');
	fm_total_download=models.IntegerField(default=0, verbose_name='Total download');
	fm_search_tag=models.TextField(verbose_name='Tags', blank=True);
	def __str__(self):
		return self.fm_name;

class StockRecord(models.Model):
    stock_id=models.AutoField(primary_key=True, unique=True, verbose_name='Stock ID');
    stock_date_time=models.DateTimeField(auto_now_add=True, verbose_name='Date/Time');
    stock_value=models.FloatField(verbose_name='Value');
    stock_change=models.FloatField(verbose_name='Change');
    def __str__(self):
        return '{} - {}'.format(self.stock_date_time, self.stock_value);