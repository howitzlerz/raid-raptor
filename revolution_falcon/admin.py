from django.contrib import admin
from revolution_falcon.models import FileManager, StockRecord;
# Register your models here.
admin.site.register(FileManager);
admin.site.register(StockRecord);
