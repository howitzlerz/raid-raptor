from django.forms import ModelForm, PasswordInput;
from revolution_falcon.models import FileManager, IOT, ServiceLog, Student, Card, Parent, Attendance, Food, Transaction, LineItem, Tracker;
from django.views.generic.edit import CreateView

class IOT_Form(ModelForm):
    class Meta:
        model=IOT;
        fields=['iot_uid', 'iot_name', 'iot_location', 'iot_mode' ,'iot_status'];

class Student_Form(ModelForm):
    class Meta:
        model=Student;
        fields='__all__';
        exclude=['stud_id'];
        
class Parent_Form(ModelForm):
    class Meta:
        model=Parent;
        fields='__all__';
        exclude=['parent_id'];
        widgets={
            'parent_password': PasswordInput(),
        };
        
class Card_Form(ModelForm):
    class Meta:
        model=Card;
        fields=['card_iot', 'card_stud'];
        
class Food_Form(ModelForm):
    class Meta:
        model=Food;
        fields='__all__';
        exclude=['food_id'];

class LineItem_Form(ModelForm):
    class Meta:
        model=LineItem;
        fields='__all__';
        exclude=['line_tran'];
        
class Transaction_Form(ModelForm):
    class Meta:
        model=Transaction;
        fields=['tran_iot'];