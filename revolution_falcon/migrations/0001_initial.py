# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FileManager',
            fields=[
                ('fm_id', models.AutoField(unique=True, serialize=False, verbose_name='File ID', primary_key=True)),
                ('fm_name', models.CharField(max_length=32, verbose_name='File Name')),
                ('fm_group_name', models.CharField(max_length=32, blank=True, choices=[('Document', 'Document'), ('Video', 'Video'), ('Music', 'Music'), ('Picture', 'Picture'), ('Others', 'Others')])),
                ('fm_short_url', models.CharField(max_length=127, blank=True, verbose_name='File URL')),
                ('fm_file', models.FileField(upload_to='raidraptor/templates/files/', verbose_name='File')),
                ('fm_password', models.CharField(max_length=32, blank=True, verbose_name='Password', default='')),
                ('fm_total_download', models.IntegerField(verbose_name='Total download', default=0)),
                ('fm_search_tag', models.TextField(blank=True, verbose_name='Tags')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StockRecord',
            fields=[
                ('stock_id', models.AutoField(unique=True, serialize=False, verbose_name='Stock ID', primary_key=True)),
                ('stock_date_time', models.DateTimeField(auto_now_add=True, verbose_name='Date/Time')),
                ('stock_value', models.FloatField(verbose_name='Value')),
                ('stock_change', models.FloatField(verbose_name='Change')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
